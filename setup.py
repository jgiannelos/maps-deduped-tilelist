from setuptools import find_packages, setup

setup(
    name="wmf-maps-tools",
    version="0.0.5",
    author="Yiannis Giannelos",
    author_email="jgiannelos@wikimedia.org",
    description="Collection of WMF maps related python utils",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(include=["wmfmaps.*"]),
    install_requires=["pyyaml", "osmium", "psycopg2"],
    python_requires=">=3.7",
    entry_points={
        "console_scripts": [
            "maps-deduped-tilelist=wmfmaps.tileset.cli:main",
            "imposm-changeset-verify=wmfmaps.changeset.cli:main",
        ],
    },
)
