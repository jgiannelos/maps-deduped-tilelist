stages:
  - lint
  - test
  - package
  - release

lint:
  stage: lint
  image: python:latest
  before_script:
    - pip install flake8
  script:
    - flake8 .
  tags:
    - maps

test:
  stage: test
  image: python:latest
  script:
    - pip install .
    - python -m unittest discover -v
  tags:
    - maps

build_python:
  stage: package
  image: python:latest
  script:
    - python setup.py sdist
    - cp dist/wmf-maps-tools*.tar.gz .
  artifacts:
    paths:
      - wmf-maps-tools*.tar.gz
  tags:
    - maps

build_debian:
  stage: package
  image: debian:stable
  script:
    - apt-get update
    - apt-get -y install python3-all python3-setuptools dh-make dh-python
    - dpkg-buildpackage -b -us -uc
    - mv ../python3-maps-deduped-tilelist*.deb .
  artifacts:
    paths:
      - python3-maps-deduped-tilelist*.deb
  tags:
    - maps

publish_debian_package:
  rules:
    - if: $CI_COMMIT_TAG
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Ensure debian artifacts don't expire for $CI_COMMIT_TAG"
  needs:
    - job: build_debian
      artifacts: true
  artifacts:
    paths:
      - python3-maps-deduped-tilelist*.deb
    expire_in: never
  tags:
    - maps

publish_python_package:
  rules:
    - if: $CI_COMMIT_TAG
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Ensure python artifacts don't expire for $CI_COMMIT_TAG"
  needs:
    - job: build_python
      artifacts: true
  artifacts:
    paths:
      - wmf-maps-tools*.tar.gz
    expire_in: never
  tags:
    - maps

release:
  rules:
    - if: $CI_COMMIT_TAG
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: publish_debian_package
    - job: publish_python_package
  script:
    - echo 'Create a new release for $CI_COMMIT_TAG'
  release:
    tag_name: "$CI_COMMIT_TAG"
    description: "$CI_COMMIT_TAG"
    assets:
      links:
        - name: Debian package
          url: "$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/artifacts/$CI_COMMIT_TAG/download?job=publish_debian_package"
          link_type: package
        - name: Python package
          url: "$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/artifacts/$CI_COMMIT_TAG/download?job=publish_python_package"
          link_type: package
  tags:
    - maps
