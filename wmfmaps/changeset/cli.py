import argparse

from wmfmaps.changeset.verifier import Verifier


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("changeset")
    parser.add_argument("mapping")
    parser.add_argument("pgURL")
    parser.add_argument("--failfast", action="store_true")
    args = parser.parse_args()

    verifier = Verifier(args.changeset, args.mapping, args.pgURL, args.failfast)
    verifier.verify()
