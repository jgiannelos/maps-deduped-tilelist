import json
import osmium
import psycopg2
import yaml


class InconsistentChangeException(Exception):
    pass


class ElementHandler:
    def __init__(self, conn, tables, failfast):
        self.conn = conn
        self.tables = tables
        self.failfast = failfast
        self.inconsistencies = []

    def serialize(self, change):
        tags = {}
        for tag in change.tags:
            tags[tag.k] = tag.v

        return {
            "id": change.id,
            "changeset": change.changeset,
            "version": change.version,
            "tags": tags,
        }

    def exists(self, table, osm_id):
        query = f"SELECT * FROM {table} WHERE osm_id = {osm_id}"
        cursor = self.conn.cursor()
        cursor.execute(query)
        return cursor.fetchone() is not None

    def verify_deleted(self, change):
        for table in self.tables:
            if self.exists(table, change.id):
                if self.failfast:
                    raise InconsistentChangeException(
                        f"Element with id:{change.id} shouldn't exist"
                    )
                self.inconsistencies.append(
                    {"type": "should be deleted", "element": self.serialize(change)}
                )
                break

    def verify_existing(self, change):
        at_least_once = False
        for table in self.tables:
            if self.exists(table, change.id):
                at_least_once = True
                break

        if not at_least_once:
            if self.failfast:
                raise InconsistentChangeException(
                    f"Element with id:{change.id} should exist"
                )
            self.inconsistencies.append(
                {"type": "should exist", "element": self.serialize(change)}
            )

    def log_inconsistencies(self):
        for elem in self.inconsistencies:
            print(f"{json.dumps(elem)}")

    def __call__(self, change):
        if change.deleted:
            self.verify_deleted(change)
        else:
            self.verify_existing(change)


class Verifier:
    def __init__(self, changesetPath, mappingPath, pgURL, failfast=True):
        self.changesetPath = changesetPath
        self.mappingPath = mappingPath
        self.pgURL = pgURL
        self.tables = self.get_tables()
        self.conn = psycopg2.connect(self.pgURL)
        self.conn.set_session(readonly=True)
        self.failfast = failfast

    def get_tables(self):
        with open(self.mappingPath, "r") as f:
            mapping = yaml.safe_load(f)
            return mapping["tables"].keys()

    def verify(self):
        node = ElementHandler(self.conn, self.tables, self.failfast)
        way = ElementHandler(self.conn, self.tables, self.failfast)
        relation = ElementHandler(self.conn, self.tables, self.failfast)
        handler = osmium.make_simple_handler(node=node, way=way, relation=relation)
        handler.apply_file(self.changesetPath)

        node.log_inconsistencies()
        way.log_inconsistencies()
        relation.log_inconsistencies()
